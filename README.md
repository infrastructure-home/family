Ansible setup for the devices of my family.

# Testing
As all my family members are either emotionally damaged by Microsoft or Apple, the best OS for them is ZorinOS.  
Therefore use a ZorinOS installation in virtual box for testing the Ansible scripts.

```bash
ansible-playbook --ask-vault-pass -i staging site.yml
```

# How to set up a new device
- Install OS with the admin account
- Manual install a ssh server with defaults (password authentication on)
    ```bash
    sudo apt install openssh-server
    ```
- Create a unique ssh key pair for this device
- exchange key files over ```ssh-copy-id -i <key_file> <admin-user>@<ip>```
- Set up a host var file for the device and also add it to the desired production groups
- Execute playbook for production limited to the device
    ```bash
    ansible-playbook --ask-vault-pass -i production -l <device> site.yml
    ```

Note: there is no need to manual take care of the ssh hardening, as this is covered by an ansible task
